//
//  KMPageUpdater.h
//  Kimono
//
//  Created by James Dumay on 8/04/12.
//  Copyright (c) 2012 Atlassian. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "KMBrowserTab.h"

@class KMPage;

@protocol KMPageUpdater <NSObject>

- (void)update:(id<KMBrowserTab>)page;

@end
