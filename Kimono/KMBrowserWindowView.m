//
//  KMBrowserWindowView.m
//  Kimono
//
//  Created by James Dumay on 27/08/12.
//  Copyright (c) 2012 Whimsy. All rights reserved.
//

#import <QuartzCore/QuartzCore.h>

#import "KMBrowserWindowView.h"
#import "KMBrowserWindowController.h"


@implementation KMBrowserWindowView

@synthesize toolbarArea = _toolbarArea;

-(KMBrowserToolbar*)toolbar
{
    KMBrowserWindowController *controller =  (KMBrowserWindowController*)[[self window] windowController];
    return [controller toolbar];
}

-(NSView *)innerView
{
    KMBrowserWindowController *controller =  (KMBrowserWindowController*)[[self window] windowController];
    return [controller innerView];
}

-(void)mouseEntered:(NSEvent *)theEvent
{
    KMBrowserWindowController *controller =  (KMBrowserWindowController*)[[self window] windowController];
    [[self toolbar] showWithDelay:[controller innerView]];
}

-(void)mouseExited:(NSEvent *)theEvent
{
    [[self toolbar] hide];
}

-(void)updateTrackingAreas
{
    [super updateTrackingAreas];
    
    if (_toolbarArea != nil)
    {
        [self removeTrackingArea:_toolbarArea];
    }
    
    NSRect toolbarFrame = NSMakeRect([self innerView].frame.origin.x, [self innerView].frame.size.height - 36, [self innerView].frame.size.width, 36);
    
    _toolbarArea = [[NSTrackingArea alloc] initWithRect:toolbarFrame options:(NSTrackingMouseEnteredAndExited|NSTrackingCursorUpdate|NSTrackingActiveInActiveApp) owner:self userInfo:nil];
    
    [self addTrackingArea:_toolbarArea];
}


@end
